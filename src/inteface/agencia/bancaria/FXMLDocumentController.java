/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inteface.agencia.bancaria;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Leonardo
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    
    @FXML
    private TextField nome;
    
    @FXML
    private TextField endereço;
    
    @FXML
    private TextField cidade;
    
    @FXML
    private TextField bairro;
    
    @FXML
    private TextField estado;
    
    @FXML
    private TextField telefone;
    
    @FXML
    private TextField email;
    
    @FXML
    private TextField cpf;
    
    @FXML
    private TextField rg;
    
    @FXML
    private TextField titular;
    
    @FXML
    private TextField numero;
    
    @FXML
    private TextField digito;
    
    @FXML
    private TextField saldo;
     
    @FXML
    private void Credita(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @FXML
    private void Debita(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
